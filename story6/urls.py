from django.conf.urls import url
from .views import landingpage, profile

urlpatterns = [
	url(r'^$', landingpage , name='landingpage'),
	url(r'^profile', profile , name='profile')

]