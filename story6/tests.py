from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import landingpage, profile

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


    
    # Create your tests here.
class story6UnitTest(TestCase):
    
    def test_story_6_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_story_6_url_is_exist2(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    #def test_story_6_url_is_exist(self):
        #response = Client().get('/story6/')
        #self.assertTemplateUsed(response, 'landingpage.html' )
    
    def test_story_6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, landingpage)

    def test_story_6_using_index_func2(self):
        find = resolve('/profile/')
        self.assertEqual(find.func, profile)
    



# Create your tests here.
class Lab7FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        super(Lab7FunctionalTest, self).setUp()
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super(Lab7FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://hidupdemippw.herokuapp.com/')
        # find the form element
        status = selenium.find_element_by_id('id_mood')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        status.send_keys('Coba Coba')
        time.sleep(5)

        # submitting the form
        submit.send_keys(Keys.RETURN)
        time.sleep(5)

    def posisi_layout_satu(self):
        selenium = self.selenium
        selenium.get('http://hidupdemippw.herokuapp.com/')
        header_text = selenium.find_element_by_tag_name('p').text
        self.assertIn('jasmine', header_text)

    def posisi_layout_dua(self):
        selenium = self.selenium
        selenium.get('http://hidupdemippw.herokuapp.com/')
        my_header = selenium.find_element_by_tag_name('p').text
        self.assertIn('feel', my_header)

    def test_css_class_insiderec(self):
        selenium = self.selenium
        selenium.get('http://hidupdemippw.herokuapp.com/')
        clear_button = selenium.find_element_by_class_name('insiderec').value_of_css_property('padding')
        self.assertIn('30px', clear_button)

    def test_css_class_roundedrec(self):
        selenium = self.selenium
        selenium.get('http://hidupdemippw.herokuapp.com/')
        rounded = selenium.find_element_by_class_name('roundedrec').value_of_css_property('border-radius')
        self.assertIn('10px',rounded)

if __name__ == '__main__' :
    unittest.main(warnings='ignore')
