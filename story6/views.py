from django.shortcuts import render
from .forms import FormKabar
from .models import Kabar


def landingpage(request):
	form = FormKabar(request.POST or None)
	response = {}
	kabar2 = Kabar.objects.all()
	response = {
		"kabar2" : kabar2
	}
	response['form'] = form

	if(request.method == "POST" and form.is_valid()):
		mood = request.POST.get("mood")
		Kabar.objects.create(mood=mood)
	return render(request, 'landingpage.html', response)

def profile(request):
	return render(request, "profile.html")
		